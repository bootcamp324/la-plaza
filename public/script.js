for (let i=1; i<19; i++) {
    arrastrarElemento(document.getElementById(`plaza${i}`))
}

function arrastrarElemento(elementoDePlaza) {
    
    //establecer 4 posiciones para posicionar en la pantalla
    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

    elementoDePlaza.onpointerdown = arrastrarPuntero;

    function arrastrarPuntero(e) {
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onpointermove = arrastrarElemento;
        document.onpointerup = detenerArrastreElemento;
    }

    function arrastrarElemento(e) {
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        elementoDePlaza.style.top = elementoDePlaza.offsetTop - pos2 + 'px';
        elementoDePlaza.style.left = elementoDePlaza.offsetLeft - pos1 + 'px';
        elementoDePlaza.style.margin = 0;
        
    }

    function detenerArrastreElemento() {
        document.onpointerup = null;
        document.onpointermove = null;
    }

}